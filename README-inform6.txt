Hello! If you want to translate a game made with Inform 6, you've come to
the right place! Here's a quick tutorial on how to use my script, the
aptly-named scriptTradInform6.py !

== How it works ==

The script extracts for you all the text that can be displayed to the
player, plus the words that the parser will recognize. It puts it in a
separate file, so you can work on it without touching the game's code. Once
you're done, the script will generate the final game file for you!

The alternative is to directly mess with code; but mistakes can happen, and
the final game may be broken! Not even mentioning the fact that not
everyone knows how (or wants) to code in Inform 6!

With this script, you'll still need to tweak a few things at the end; i'll
explain how, but this requires a bit of Inform 6 knowledge. Nothing major,
but if you're inexperienced, you may need help for these final adjustments!


== What you need ==

Four things:
 - the source code of an Inform 6 game ;
 - the Inform 6 compiler ;
 - the Inform 6 librairies of the target language (about a dozen are
   available!) ;
 - the programming language Python 3: download it at python.org, it is
   needed to run the script.


== Using the script ==

=== Starting from scratch ===

If you're on Windows: TODO
If you're on Linux or OSX: open a terminal and go to the folder your game
source is in. Put also the scriptTradInform6.py file in that folder.

Say the source for your game is named myGame.inf. First, you're going to
create two things:
 - a translation file, named myGame.po
 - an output file, named myTranslatedGame.inf

The command for this is
  python scriptTradInform6.py -i myGame.inf -t myGame.po -o myTranslatedGame.inf

In the above, -i stands for input, -t is for the translation file, -o is
for the output (where the final, translated game will be written).

Careful! There is still a long way before the translated game is playable,
or compiles, even!

=== What is this .po file ? ===

A .po file is a translation file, using the PO format which is fairly
standard. You will easily find software that allows you to edit these files
pretty conveniently; for instance there is POEdit, POEditor, Virtaal, etc.

But you can also just use a plain old text editor! Let me walk you through
it. Open the translation file myGame.po. Ignore the first group of lines;
the rest should look like

# myGame.inf:44
msgid "You open the chest -- it's empty!"
msgstr "[TRA]"

Your translation just goes where the [TRA] is. The ":44" just tells you
that the original string is at line 44 (ish) in myGame.inf: it's very
useful if you want to get more context to choose the right way to translate
something.

As a general rule of thumb, your translation should follow the same format
as the original: use ~ for when there are quotation marks, use ^ for line
breaks when there are some, etc.

Note: throughout translation you can attempt to compile, but it probably
won't work until:
 - you have translated all the verbs (otherwise you get 'multiple
definitions of the verb [TRA]') ;
 - you have given a 'translation' to the library names: usually it'll be
   'Parser' for 'Parser', 'Verblib' for 'Verblib', and the grammar file of the
   target language (for instance 'FrenchG') instead of the grammar file of the
   source language (for instance 'Grammar').
Once this is done, compilation will probably be successful, but the result
will not look playable for much of the time!


=== Once you're done ===

Once you have the .po file all done, run the exact same command as the
beginning:
  python scriptTradInform6.py -i myGame.inf -t myGame.po -o myTranslatedGame.inf

This will give you the final file! You can save the .po file somewhere: you
don't need it anymore, but if the game gets updated, this will give you a
very good start to translate the update.

=== Finishing touches ===

You're almost done, but may still have bugs! Just a little bit of extra
work is needed to make this perfect. For all this, I recommend not to work
on the .po file, as most of the corrections you'll be making are to
counteract downsides of the format. I recommend working directly on your
translated source code, but keeping a list of changes you've made
somewhere, just in case (for instance if the original game is updated, you
don't want to re-do it all!)

First, you should check all the 'name' properties of the objects in the
code. Each is of the form
  name 'tie' 'dotted' 'red' 'hideous'
That is, a list of single words separated by single quotes. The translation
process can create three kinds of bugs: two words in between single quotes
(you just have to splice it), a bad translation (here, 'tie' is an item of
clothing but maybe you also have the verb 'tie' in the code, and since
there is only 1 translation per string... you have to fix it by hand!), or
a missing synonym (some languages have more synonyms for a word - don't
forget to add them!). In each case, the errors are easy to fix.

Then, you should check the grammar. Since a grammatical line in I6 is
defined using 'individual' 'words' 'separated' 'by' 'quotes', the
translation process will have made you translate them one by one; and
word-by-word translation rarely works! Much as with the names, you need to
look into these lines, changing them to reflect a more natural way to say
it, adding alternative ways to say it, etc. This might require some help;
I6 grammar is powerful but subtle!

Finally, you should look at any parse_name routines; much as with the
grammar lines, these hardcode individual words in a specific order, and
what's thus recognized is often not the right way to express it in the
target language!

== That's all folks! ==

Happy translating! I hope this script will be useful for you, and don't
hesitate to get in touch for troubleshooting, a bug report, or even help
with the development!

