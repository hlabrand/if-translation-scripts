#!/usr/bin/python


#=======================================
#       scriptTradTwine.py
#
#  By: Hugo Labrande (first name at hlabrande dot fr)
#  Licence: public domain
#  Version 1 (Feb 15th 2017)
#
#  This is inspired by scriptTradInform.py, which predates it by 2 years.
#
#
#
#  How to run:
#    1/ make sure you can run Python scripts (you need Python)
#    2/ from command line:
#        python scriptTrad.py -i <sourceToBeTranslated> -t <yourTranslation>.po -o <translatedSource>
#       Note that the translation file is a .po file, which is a widely used internationalization format.
#
#  What it does:
#    Reads the .po file (if it exists) and stores it somewhere, then creates the file specified in the -o argument
#        by copying the original and replacing all the strings by the .po's translated strings.
#    For now, only supports one translation per string.
#
#  How to translate a game:
#     1/ Run the script with the source of the game as the '-i' argument
#     2/ Edit the .po file (using a text editor or a more specialized tool like POedit). A few tips:
#          a. Try to stick to the same format: in particular, put spaces where there were spaces, etc.
#             Do not put more than one blank line between two groups of 'original+new' strings, it may
#             result in ugly things (though most probably harmless) in the translated source code.
#          b. If you see weird characters (&quot;, &lt; &gt;, etc.), leave them in in: those are double
#             quotes or HTML tags as written in Twine, and are needed for the game to have the same 'look'.
#     3/ The game should run, but there could be some extra strings that were missed (for instance the
#          title, or other strings specified in javascript code outside the passages)
#          For these, unfortunately you'll have to do it by hand! Read the HTML code, or play the game till
#          you find an untranslated string and find it in the HTML code.
#
#  Change this variable to 0 if you want everything that isn't translated yet to be replaced by _TRA_ in the
#     output file. This will make it easy to spot on readings, but may break some stuff (like calls to passage names).
#  If this variable is 1, a string that has no translation yet will be left like that in the output. This ensures
#     the game is still playable, and you can also check for context; but the output will be a mix of both languages.
leaveUntranslatedStringsAlone = 1


# TODO list:
#   several translations for one string using msgctxt)
#   line numbers and strings appearing several times
#   do we want to add any headers to the .po file? what is the convention here?




emptyTrad = '_TRA_'


import sys, getopt

#===============================================================================================
# Deal with command-line arguments
argv = sys.argv[1:]
try:
    opts, args = getopt.getopt(argv,"hi:t:o:l:",["input=","translated=", "output=", "language="])
except getopt.GetoptError:
    print ('scriptTradTwine.py -i <sourceToBeTranslated> -t <yourTranslationFile> -o <translatedSource>')
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        print ('scriptTradTwine.py -i <sourceToBeTranslated> -t <yourTranslationFile> -o <translatedSource>')
        sys.exit()
    elif opt in ("-i", "--ifile"):
        sourceToBeTranslated = arg
    elif opt in ("-t", "--translated"):
        translationFile = arg
    elif opt in ("-o", "--ofile"):
        translatedSource = arg
print ('Attempting to read a Twine HTML file')
print ('Code to be translated :', sourceToBeTranslated)
print ('(partial) Translation file :', translationFile)
print ('Translated source :', translatedSource)


#===============================================================================================
# The program:
#  - loads the dictionary of partially translated sentences
#  - completes the dictionary by reading the source code
#  - outputs the code where all the strings have been replaced by their translations


didwecreatePO = 0

# Create the dictionary
translation = {}

#===============================================================================================
print("Loading dictionary in the translation file...")
# Load the dictionary of partially translated sentences
try:
    partial = open(translationFile)
except:
    partial = open(translationFile, "w")
    partial.close
    partial = open(translationFile)
    print("Translation file doesn't exist: creating it.")
    didwecreatePO = 1


# This is a dumb .po parser
# keep what you read in a string since open with "w" overwrites it
partialstring = ""
FLAG_MSGID = 0
FLAG_MSGSTR = 1
FLAG_MSGCTXT = 2
currentstr = ["", "", ""]
current_flag = FLAG_MSGID
line = partial.readline()
while line != '':
      
    partialstring += line
      
    # skip the initial spaces of the line
    line = line.strip(" ")
    
    # skip if newline or comment line
    #   (no need to parse the 'warning it may be a passage name',
    #    since it should only be there if the HTML has that passage, nothing else)
    if (line == '\n' or line[0] == '#'):
        line = partial.readline()
        continue
    
    # if this is the continuation of a message
    if (line[0] == '\"'):
        # strip the quotes
        line = line.strip('\n')
        line = line.strip(' ')
        line = line.strip('\"')
        # add it to the current message
        currentstr[current_flag] += '\n'
        currentstr[current_flag] += line
        line = partial.readline()
        continue
      
    if (line[0:3] == "msg"):
        if (line[0:5] == "msgid"):
            # save the previous set "currentstr"	#FIXME: right now we only support one translation per string, but this is could be more subtle (and more powerful)
            translation[currentstr[FLAG_MSGID]] = currentstr[FLAG_MSGSTR]
            # begin parsing of the new stuff
            currentstr = ["", "", ""]
            current_flag = FLAG_MSGID
            # delete msgid and the trailing newline
            line = line[5:-1]
            line = line.strip(' ')
            line = line.strip('\"')
            currentstr[current_flag] = line
            line = partial.readline()
            continue
        if (line[0:6] == "msgstr"):
            current_flag = FLAG_MSGSTR
            # delete msgstr and the trailing newline
            line = line[6:-1]
            line = line.strip(' ')
            line = line.strip('\"')
            currentstr[current_flag] = line
            line = partial.readline()
            continue
        if (line[0:7] == "msgctxt"):
            current_flag = FLAG_MSGCTXT
            # delete msgid and the trailing newline
            line = line[7:-1]
            line = line.strip(' ')
            line = line.strip('\"')
            currentstr[current_flag] = line
            line = partial.readline()
            continue
    
    line = partial.readline()

partial.close()
# save the last ones   #FIXME: right now we only support one translation per string, but this is could be more subtle (and more powerful)
translation[currentstr[FLAG_MSGID]] = currentstr[FLAG_MSGSTR]


#print(translation)
#sys.exit()


#===============================================================================================
# Building passage names list
print("Building list of passage names, to be flagged should they come up in the translation file...")
f = open(sourceToBeTranslated)
sourceContents = f.read()
# Regexp: tiddler="passage"
import re
listOfPassages = re.findall('tiddler=".*?"', sourceContents)
f.close()
# Extract the actual passage name
listOfPassages = [ (listOfPassages[i].split('"'))[1] for i in range(0,len(listOfPassages)) ]
#print(listOfPassages)
#sys.exit()


#===============================================================================================
print("Reading the source, updating the translation file and writing the translated output...")
# Read the file
f = open(sourceToBeTranslated)
# Erases the previous translation file but writes it again
partial = open(translationFile, "w")
partial.write(partialstring)
# Always a new file
output = open(translatedSource, "w")

if (didwecreatePO == 1):
    # Write the headers of the PO file
    partial.write("# Translation of the strings in the Twine HTML source code file \""+sourceToBeTranslated+"\".\n#\n")
    partial.write("# File generated using Hugo Labrande's scriptTrad.py (www.hlabrande.fr/if).\n\n")
    partial.write("msgid \"\"\n")
    # which headers should we write?!
    partial.write("msgstr \"\"\n\"Content-Type: text/plain; charset=UTF-8\\n\"\n")
  
  
# Helper function
def multilinePOprint(mystr, myfile):
    "This prints a string in a (.po) file, using the quotes for multi-line strings"
    for ch in mystr:
        if (ch == '\n'):
            myfile.write("\"\n\"")
        else:
            myfile.write(ch)
    return

# keeping track of the line number
linenumber = 1;
def onemorechar(myfile):
    "Gives you one more char, while updating the global variable that keeps track of the number of lines"
    global linenumber
    ch = f.read(1)
#    if (linenumber == 1):
#        print (ch)
    if (ch == '\n'):
        linenumber += 1
    return ch

# write a string in the dictionary + its translation in the output
def writeDictionaryString():
    global dictionaryString
    global translation
    global nbrstrings
    
    if (dictionaryString == ""):
        output.write("")
    elif (dictionaryString == "\n"):
        output.write('\n')
    else:
        # if it's not in the dictionary, add it and write in the translation file
        if (dictionaryString not in translation):
            translation[dictionaryString] = emptyTrad
            nbrstrings += 1
            partial.write('\n')
            # note: the line number here is the last line of the string
            #       it's just for a hint anyway, and it'd be too much of a hassle for me to write the beginning, sorry!
            partial.write("# "+sourceToBeTranslated+":"+str(linenumber)+"\n")
            if (dictionaryString in listOfPassages):
                partial.write("# WARNING The HTML has a passage with this name. You should leave it as is, unless you're sure of what you're doing!\n")
            #partial.write("msgctxt \""+str(nbrstrings)+"\"\n")	# different context every time
            partial.write("msgid \"")
            multilinePOprint(dictionaryString, partial)
            partial.write("\"\nmsgstr \"")
            multilinePOprint(translation[dictionaryString], partial)
            partial.write('\"\n')
        # Write the translation in the output
        if (translation[dictionaryString] == emptyTrad):
            if (leaveUntranslatedStringsAlone == 0):
                # Write _TRA_ in output file
                output.write(translation[dictionaryString])
            else:
                # Leave the untranslated string in
                output.write(dictionaryString)
        else:
            # Write the translated string
            output.write(translation[dictionaryString])
        
    dictionaryString = ""

# do the thing
nbrstrings = len(translation)
## sourcestring contains code, dictionarystring contains a string between " or '
## there's always one empty
sourceString = ""
dictionaryString = ""

## first we need to get to the right line and ignore everything before
## we need to have a pointer to the beginning of the previous line
pointer = 0
line = f.readline()
while "<div id=\"passages\">" not in line:
    output.write(line)
    pointer = f.tell()
    line = f.readline()
    linenumber += 1
# rewind to get to the beginning of the line where there is div id passages
f.seek(pointer,0)
print(" (Start of passages found at line "+str(linenumber)+"; string extraction starting now)")

def dealWithLink():
    global dictionaryString
    global sourceString
    #  There are two kinds of links: [[text|passageName]] or [[passageName]]
    #     Since we can't touch passagenames for fear of breaking something, we have
    #     to transform [[passageName]] into [[passageNameTranslated|passageName]]
    writeDictionaryString()
    output.write("[[")
    c = onemorechar(f)
    while (c != '|' and c != ']'):
        dictionaryString += c
        c = onemorechar(f)
    if (c == ']'):      # save the name of the passage for displaylater
        simplepassage=1
        passagename=dictionaryString
    else:
        simplepassage=0
    writeDictionaryString()     # this resets dictionaryString
    if simplepassage==0:
        sourceString = ""
        while (c != ']'):
            sourceString += c
            c = onemorechar(f)
        sourceString += "]"
    else:
        sourceString = "|"+passagename+"]"
    # we don't want to have \n in the name of the passage (it's not a valid passage name)
    sourceString = sourceString.replace("\n","")
    c = onemorechar(f)
    if (c == ']'):
        # phew, we're done
        sourceString += c
        output.write(sourceString)
    else:
        # code executed upon clicking on the link ("setter-links")
        sourceString += c
        # here comes some code : extract all that is between quotation marks
        insideAQuote = 0
        while (c != ']'):
            c = onemorechar(f)
            if (c == '"'):
                if (insideAQuote == 0):
                    insideAQuote = 1
                    output.write(sourceString+'"')
                    sourceString=""
                else: # leaving a quote
                    insideAQuote = 0
                    writeDictionaryString()
                    sourceString = ""
                    sourceString += c
            else:
                if (insideAQuote == 0):
                    sourceString += c
                else:
                    dictionaryString += c
        c = onemorechar(f) # spend the other ] (at this point it's gotta be a ])
        sourceString += ']'
        output.write(sourceString)
    sourceString = ""
def dealWithCode():
    #global partial
    #global output
    global dictionaryString
    global sourceString
    #global f
    # we just saw a &, so we may be entering some code? (of the form "&gt; &gt; statement &lt; &lt;" in the HTML)
    #    we attempt to determine if we're in code, then if we are, only take what's in quotes
    pointer = f.tell()
    mystr = f.read(7)
    if (mystr != "lt;&lt;"):
        f.seek(pointer,0)
        dictionaryString += "&"
        return
    # if it's indeed "&lt;&lt;"
    writeDictionaryString()
    sourceString = "&lt;&lt;"
    flag = 0
    inQuotes = 0
    while (flag == 0):
        c = onemorechar(f)
        if (c == '"'):
            if (inQuotes == 0):
                output.write(sourceString)
                output.write('"')
                sourceString = ""
                inQuotes = 1
            else:
                writeDictionaryString()
                inQuotes = 0
                sourceString = ""
                sourceString += '"'
        elif (c == '&'):
            if (inQuotes == 1): # don't bother if you're in quotes
                dictionaryString += c
            else:
                pointer = f.tell()
                mystr = f.read(7)
                if (mystr != "gt;&gt;"):
                    f.seek(pointer, 0)
                    sourceString += c
                else: # found the end
                    sourceString += "&gt;&gt;"
                    output.write(sourceString)
                    sourceString = ""
                    return
        else:
            if (c == ''):
                return
            else:
                if (inQuotes == 0):
                    sourceString += c
                else:
                    dictionaryString += c
                    
def dealWithImage():
    global dictionaryString
    global sourceString
    writeDictionaryString()
    sourceString = "[img["
    count=0
    while (count != 2):
        c = onemorechar(f)
        if (c == ']'):
            count += 1
        else:
            count = 0
            sourceString += c
    output.write(sourceString+"]]")
    sourceString=""
    return 
        
def dealWithScript():
    global dictionaryString
    global sourceString
    writeDictionaryString()
    sourceString = "<script"
    flag =0
    while (flag == 0):
        c = onemorechar(f)
        if (c == '<'):
            pointer = f.tell()
            if (f.read(8) == "/script>"):
                sourceString += "</script>"
                return
            else:
                pointer = f.seek(pointer,0)
                sourceString += c
        else:
            sourceString += c

c = onemorechar(f)
## from now on, we just have to ignore all the tags, starting with what's in line
insideATag = 0
linkbracket = 0
while (c != ''):
    if (c == '<'):
        pointer = f.tell()
        if (f.read(6) == "script"):
            # ignore everything between <script> </script>, don't translate it
            dealWithScript()
        else:
            f.seek(pointer,0)
            insideATag=1
            writeDictionaryString()
            sourceString = "<"
    elif (c == '>'):
        sourceString += '>'
        insideATag=0    # assuming you can't nest tags in HTML?
        output.write(sourceString)
        sourceString=""
    elif (c == '['):        # [[text | link ]] or [img[thing.png]]
        if (insideATag == 1): # can this happen? a square bracket in html code?
            sourceString += c
        else:
            c = onemorechar(f)
            if (c == '['):
                # two brackets in a row: a link
                dealWithLink()
            elif (c == 'i'):
                # could be an image
                pointer = f.tell()
                if (f.read(3) != "mg["):
                    # oh well never mind, rewind
                    f.seek(pointer,0)
                    dictionaryString += "[i"
                else:
                    # it's an image
                    dealWithImage()
            else:
                # oh well never mind
                dictionaryString += '['
                dictionaryString += c
    elif (c == '&'):
        if (insideATag == 0): # it could be twine code!
            dealWithCode()
        else:
            sourceString += c
    else:
        if (insideATag == 1):
            sourceString += c
        else:
            # replace " by &quot; in strings that end up in the .po file
            if (c == "\""):
                c = "&quot;"
            dictionaryString += c
                
    c = onemorechar(f)


# The end: presumably all flags are down since syntax is good
output.write(sourceString)


f.close()
partial.close()
output.close()



partial = open(translationFile)
string = partial.read()
print("Done:",string.count(emptyTrad), "strings left to translate.")
partial.close()
