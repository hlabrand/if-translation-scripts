#!/usr/bin/python


#=======================================
#       scriptTradInform7.py
#
#  By: Hugo Labrande (first name at hlabrande dot fr)
#  Licence: public domain
#  Version 2.02 (Feb 12th 2017): refactoring + space vs tabs
#    ( Version 2.01 (July 3rd 2016): fixed a huge bug with single quotes
#      Version 2 (March 6th 2016) : replaced .trad files by .po files
#      Version 1 (May 24th 2015)
#    )
#
#  See the README-inform7.txt from the same repository!
#
#


# TODO list:
#   several translations for one string using msgctxt)
#   line numbers and strings appearing several times
#   do we want to add any headers? what is the convention here?



emptyTrad = '[TRA]'


import sys, getopt

#===============================================================================================
# Deal with command-line arguments
argv = sys.argv[1:]
try:
      opts, args = getopt.getopt(argv,"hi:t:o:",["input=","translated=", "output="])
except getopt.GetoptError:
      print ('scriptTradInform7.py -i <sourceToBeTranslated> -t <yourTranslationFile> -o <translatedSource>')
      sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        print ('scriptTradInform7.py -i <sourceToBeTranslated> -t <yourTranslationFile> -o <translatedSource>')
        sys.exit()
    elif opt in ("-i", "--ifile"):
        sourceToBeTranslated = arg
    elif opt in ("-t", "--translated"):
        translationFile = arg
    elif opt in ("-o", "--ofile"):
        translatedSource = arg
        
print ('Attempting to read an Inform 7 source code')
print ('Code to be translated :', sourceToBeTranslated)
print ('(partial) Translation file :', translationFile)
print ('Translated source :', translatedSource)


#===============================================================================================
# The program:
#  - loads the dictionary of partially translated sentences
#  - completes the dictionary by reading the source code
#  - outputs the code where all the strings have been replaced by their translations


didwecreatePO = 0

# Create the dictionary
translation = {}

#===============================================================================================
print("Loading dictionary in the translation file...")
# Load the dictionary of partially translated sentences
try:
    partial = open(translationFile)
except:
    partial = open(translationFile, "w")
    partial.close
    partial = open(translationFile)
    print("Translation file doesn't exist: creating it.")
    didwecreatePO = 1


# This is a dumb .po parser
# keep what you read in a string since open with "w" overwrites it
partialstring = ""
FLAG_MSGID = 0
FLAG_MSGSTR = 1
FLAG_MSGCTXT = 2
currentstr = ["", "", ""]
current_flag = FLAG_MSGID
line = partial.readline()
while line != '':
      
    partialstring += line
      
    # skip the initial spaces of the line
    line = line.strip(" ")
    
    # skip if newline or comment line
    if (line == '\n' or line[0] == '#'):
        line = partial.readline()
        continue
    
    # if this is the continuation of a message
    if (line[0] == '\"'):
        # strip the quotes
        line = line.strip('\n')
        line = line.strip(' ')
        line = line.strip('\"')
        # add it to the current message
        currentstr[current_flag] += '\n'
        currentstr[current_flag] += line
        line = partial.readline()
        continue
      
    if (line[0:3] == "msg"):
        if (line[0:5] == "msgid"):
            # save the previous set "currentstr"	#FIXME: right now we only support one translation per string, but this is could be more subtle (and more powerful)
            translation[currentstr[FLAG_MSGID]] = currentstr[FLAG_MSGSTR]
            # begin parsing of the new stuff
            currentstr = ["", "", ""]
            current_flag = FLAG_MSGID
            # delete msgid and the trailing newline
            line = line[5:-1]
            line = line.strip(' ')
            line = line.strip('\"')
            currentstr[current_flag] = line
            line = partial.readline()
            continue
        if (line[0:6] == "msgstr"):
            current_flag = FLAG_MSGSTR
            # delete msgstr and the trailing newline
            line = line[6:-1]
            line = line.strip(' ')
            line = line.strip('\"')
            currentstr[current_flag] = line
            line = partial.readline()
            continue
        if (line[0:7] == "msgctxt"):
            current_flag = FLAG_MSGCTXT
            # delete msgid and the trailing newline
            line = line[7:-1]
            line = line.strip(' ')
            line = line.strip('\"')
            currentstr[current_flag] = line
            line = partial.readline()
            continue
    
    line = partial.readline()

partial.close()
# save the last ones   #FIXME: right now we only support one translation per string, but this is could be more subtle (and more powerful)
translation[currentstr[FLAG_MSGID]] = currentstr[FLAG_MSGSTR]


#print(translation)
#sys.exit()


#===============================================================================================
print("Reading the source, updating the translation file and writing the translated output...")
# Read the file
f = open(sourceToBeTranslated)
# Erases the previous translation file but writes it again
partial = open(translationFile, "w")
partial.write(partialstring)
# Always a new file
output = open(translatedSource, "w")

if (didwecreatePO == 1):
    # Write the headers of the PO file
    partial.write("# Translation of the strings in the I7 source code file \""+sourceToBeTranslated+"\".\n#\n")
    partial.write("# File generated using Hugo Labrande's scriptTradInform7.py (www.hlabrande.fr/if).\n\n")
    partial.write("msgid \"\"\n")
    # which headers should we write?!
    partial.write("msgstr \"\"\n\"Content-Type: text/plain; charset=UTF-8\\n\"\n")
  
  
# Helper function
def multilinePOprint(mystr, myfile):
    "This prints a string in a (.po) file, using the quotes for multi-line strings"
    for ch in mystr:
        if (ch == '\n'):
            myfile.write("\"\n\"")
        else:
            myfile.write(ch)
    return

# keeping track of the line number
linenumber = 1;
def onemorechar(myfile):
    "Gives you one more char, while updating the global variable that keeps track of the number of lines"
    global linenumber
    ch = f.read(1)
    if (ch == '\n'):
        linenumber += 1
    return ch

# write a string in the dictionary + its translation in the output
def writeDictionaryString():
    global dictionaryString
    global translation
    global nbrstrings
    
    if (dictionaryString == ""):
        output.write("")
    else:
        # if it's not in the dictionary, add it and write in the translation file
        if (dictionaryString not in translation):
            translation[dictionaryString] = emptyTrad
            nbrstrings += 1
            partial.write('\n')
            partial.write("# "+sourceToBeTranslated+":"+str(linenumber)+"\n")
            #partial.write("msgctxt \""+str(nbrstrings)+"\"\n")	# different context every time
            partial.write("msgid \"")
            multilinePOprint(dictionaryString, partial)
            partial.write("\"\nmsgstr \"")
            multilinePOprint(translation[dictionaryString], partial)
            partial.write('\"\n')
        # Write the translation in the output
        output.write(translation[dictionaryString])
    dictionaryString = ""

# do the thing
doubleQuotesFlag = 0
singleQuotesFlag = 0
## sourcestring contains code, dictionarystring contains a string between " or '
## there's always one empty
sourceString = ""
dictionaryString = ""
nbrstrings = len(translation)
c = onemorechar(f)
while (c != ''):
    # This is kinda like a finite state automata, isn't it
    if (c == "[" and singleQuotesFlag == 0 and doubleQuotesFlag == 0):
        # This is an I7 comment, don't look at it
        comment = c
        bracketLevel = 1
        # Maybe this is too complicated; after all, does I7 support brackets in brackets?
        while (bracketLevel > 0 and c != ''):
            c = onemorechar(f)
            comment = comment+c
            if (c == "["):
                bracketLevel = bracketLevel +1
            elif (c == "]"):
                bracketLevel = bracketLevel -1
        sourceString = sourceString + comment
        output.write(sourceString)
        sourceString = ""            
                
    elif (c == '"'):
        if (singleQuotesFlag == 1):
            dictionaryString = dictionaryString + c
        else:
            if (doubleQuotesFlag == 0):
                # Entering a quote
                doubleQuotesFlag = 1
                sourceString = sourceString + c
                output.write(sourceString)
                sourceString = ""
            else:
                # Leaving a quote
                doubleQuotesFlag = 0
                sourceString = c
                writeDictionaryString()

    else:
        if (doubleQuotesFlag == 1 or singleQuotesFlag == 1):
            dictionaryString = dictionaryString + c
        else:
            sourceString = sourceString + c
        
    c = onemorechar(f)

# The end: presumably all flags are down since syntax is good
output.write(sourceString)


f.close()
partial.close()
output.close()



partial = open(translationFile)
string = partial.read()
print("Done:",string.count(emptyTrad), "strings left to translate.")
partial.close()
