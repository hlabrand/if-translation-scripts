Hey there! Curious about translating a game made using Inform 7? Let me
give you a quick tutorial on how to use my script, the accurately-named
scriptTradInform7.py !

== How it works ==

The script extracts for you all the text that can be displayed to the
player, plus the words that the parser will recognize. It puts it in a
separate file, so you can work on it without touching the game's code. Once
you're done, the script will generate the final game file for you!

The alternative is to directly mess with code; but mistakes can happen, and
the final game may be broken! Not even mentioning the fact that not
everyone knows how (or wants) to code in Inform 7!

With this script, you'll still need to tweak a few things at the end; i'll
explain how, but this requires a bit of Inform 7 knowledge. Nothing major,
but if you're inexperienced, you may need help for these final adjustments!


== What you need ==

Four things:
 - the source code of an Inform 7 game ;
 - the Inform 7 compiler ;
 - the Inform 7 librairies of the target language (about half a dozen are
   available!) ;
 - the programming language Python 3: download it at python.org, it is
   needed to run the script.


== Using the script ==

=== Starting from scratch ===

If you're on Windows: TODO
If you're on Linux or OSX: open a terminal and go to the folder your game
source is in. Put also the scriptTradInform7.py file in that folder.

Say the source for your game is named myGame.ni. First, you're going to
create two things:
 - a translation file, named myGame.po
 - an output file, named myTranslatedGame.ni

The command for this is
  python scriptTradInform6.py -i myGame.ni -t myGame.po -o myTranslatedGame.ni

In the above, -i stands for input, -t is for the translation file, -o is
for the output (where the final, translated game will be written).

Careful! There is still a long way before the translated game is playable,
or compiles, even!

=== What is this .po file ? ===

A .po file is a translation file, using the PO format which is fairly
standard. You will easily find software that allows you to edit these files
pretty conveniently; for instance there is POEdit, POEditor, Virtaal, etc.

But you can also just use a plain old text editor! Let me walk you through
it. Open the translation file myGame.po. Ignore the first group of lines;
the rest should look like

# myGame.ni:44
msgid "You open the chest -- it's empty!"
msgstr "[TRA]"

Your translation just goes where the [TRA] is. The ":44" just tells you
that the original string is at line 44 (ish) in myGame.ni: it's very useful
if you want to get more context to choose the right way to translate
something.

As a general rule of thumb, your translation should follow the same format
as the original. In particular, be very careful with anything that is
written between square brackets: this is most probably code, and you should
really be careful and copy it exactly as is in your translation!


=== Once you're done ===

Once you have the .po file all done, run the exact same command as the
beginning:
  python scriptTradInform6.py -i myGame.no -t myGame.po -o myTranslatedGame.ni

This will give you the final file! You can save the .po file somewhere: you
don't need it anymore, but if the game gets updated, this will give you a
very good start to translate the update.

=== Finishing touches ===

You're almost done, but may still have bugs! Just a little bit of extra
work is needed to make this perfect. For all this, I recommend not to work
on the .po file, as most of the corrections you'll be making are to
counteract downsides of the format. I recommend working directly on your
translated source code, but keeping a list of changes you've made
somewhere, just in case (for instance if the original game is updated, you
don't want to re-do it all!)

The main thing you need to change by hand is the name of objects. An object
in Inform 7 is defined using statements such as "The skull is in the Throne
Room."; the Inform 7 compiler then automatically adds 'skull' to the list
of words that refer to this object. Hence, you need to translate the name
of the object (and maybe indicate the correct article for it as well).

Another thing to look for is the declaration of new actions: the verb being
written as plain text, and not between quotation marks, it is not captured
by the script and not written in the .po file. You thus need to go through
and change all the declarations of new verbs so that they are correctly
translated.

Both processes may give you more errors when trying to compile the game,
since all references to an object or an action become invalid as soon as
you translate them! Not to worry: the compiler displays the list of errors,
or you can try to find all occurences of the object/the action proactively.
In any case, it is recommended to deal with the errors as soon as they
appear, so you stay on top of them easier!

== That's all folks! ==

Happy translating! I hope this script will be useful for you, and don't
hesitate to get in touch for troubleshooting, a bug report, or even help
with the development!

