Do you want to translate a work in Twine into another language? Hello! Come
on in, and let me give you a quick tutorial on how to use my tool, the
blandly named scriptTradTwine.py!


== How it works ==

The script extracts for you all the text that can be displayed to the
player. It puts it in a separate file, so you can work on it without
touching the game's code. Once you're done, the script will generate the
final game file for you!

=== Comparing methods ===

Translating a Twine without my script:
 - you need the .tws file ;
 - you need to select each passage individually ;
 - you need to select the parts that are not code and write your
   translation there, without messing up the code ;
 - if sentences are repeated, you'll have to translated them over and over ;
 - if the author updates the game, can you also update without losing your
   work?

Translating a Twine with my script :
 - you just need the final html file ;
 - you get all the text and no code ;
 - you only need to translate the same sentence or string once ;
 - you can automatically apply your translation to the updated game if
   there is an update ;
 - you may need to fiddle with things a bit for the finishing touches (not
   gonna lie! :)



== What you need ==

Two things:
 - a Twine created with version 1.4 ;
 - the programming language Python 3 : download it at python.org, you will
   need it to execute the script.
Note: I haven't tried it with Twines of version 2.0, but who knows, it
might work!



== Using the script ==

=== Starting from scratch ===

If you're on Windows: TODO
If you're on Linux or OSX: open a terminal and go to the folder your game
is in. Put also the scriptTradTwine.py file in that folder.

Say your game is named myGame.html. First, you're going to create two
things:
 - a translation file, named myGame.po
 - an output file, named myTranslatedGame.html

The command for this is
  python scriptTradTwine.py -i myGame.html -t myGame.po -o myTranslatedGame.html

In the above, -i stands for input, -t is for the translation file, -o is
for the output (where the final, translated game will be written).


=== What is this .po file ? ===

A .po file is a translation file, using the PO format which is fairly
standard. You will easily find software that allows you to edit these files
pretty conveniently; for instance there is POEdit, POEditor, Virtaal, etc.

But you can also just use a plain old text editor! Let me walk you through
it. Open the translation file myGame.po. Ignore the first group of lines;
the rest should look like

# myGame.html:44
msgid "I kissed him under the rain."
msgstr "_TRA_"

Your translation just goes where the _TRA_ is. The ":44" just tells you
that the original string is at line 44 (ish) in myGame.html: it's very
useful if you want to get more context to choose the right way to translate
something.


Occasionally you may see a warning like this in the .po file:
    # WARNING The HTML has a passage with this name. You should leave it
      as is, unless you're sure of what you're doing!
This is because this (imperfect) script sometimes catches passage names,
but they shouldn't be translated, or they will break links in the game and
display a nasty "The _TRA_ passage does not exist" to the player. When
you encounter this, we advise you to not translate the string (and copy it
as it is), unless you're absolutely sure it's fine.

If there are multiple lines, the original string will be broken in multiple
lines, with quotation marks at the beginning and end of each line; follow
the same format in your translation. As a general rule of thumb, your
translation should follow the same format as the original!

Note: by default, the script replaces a string in the final HTML only if
it has been translated. If you want to change this behavior (and get a
bunch of _TRA_ in the HTML), you should open scriptTradTwine.py and
change the "leaveUntranslatedStringsAlone" variable (on line 43) to 0.


=== Once you're done ===

Once you have the .po file all done, run the exact same command as the
beginning:
  python scriptTradTwine.py -i myGame.html -t myGame.po -o
myTranslatedGame.html

This will give you the final file! You can save the .po file somewhere: you
don't need it anymore, but if the game gets updated, this will give you a
very good start to translate the update.


=== Yes but... ===

... are there bugs? (Probably! Please tell me!)

If you can still see text in the original language when playing the game:
make sure you are done with the .po file (for instance, the script said '0
strings left to translate' when you run it). If you're sure, copy the text,
open myTranslatedGame.html with a text editor, and find the text you saw on
your screen; once you see it, translate it directly, look in the few lines
before or after just to make sure there aren't others, and save. This
shouldn't happen for much text, in fact mostly just for the title!


== That's all folks! ==

Happy translating! I hope this script will be useful for you, and don't
hesitate to get in touch for troubleshooting, a bug report, or even help
with the development!

