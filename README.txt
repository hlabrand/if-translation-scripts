Welcome! Here you will find a few Python scripts to help translate IF games.

There is one script for each programming language, and a corresponding
README for each of these (the general principle is the same, but details vary!).

Currently supported:
     - Inform 6 (scriptTradInform6.py; see README-inform6.txt)
     - Inform 7 (scriptTradInform7.py; see README-inform7.txt)
     - Twine 1.4 (scriptTradTwine.py; see README-twine.txt)
     
Happy translating!
-- Hugo